
CPPFLAGS += -O2 -Wall -lzmq -L/usr/local/lib/ 

bins=zmq_push zmq_pull zmq_pub zmq_sub
all: $(bins)

zmq_push: zmq_push.cpp
	g++  $< -o $@ $(CPPFLAGS)

zmq_pull: zmq_pull.cpp
	g++  $< -o $@ $(CPPFLAGS)

zmq_pub: zmq_pub.cpp
	g++  $< -o $@ $(CPPFLAGS)

zmq_sub: zmq_sub.cpp
	g++  $< -o $@ $(CPPFLAGS)


clean:
	rm $(bins)
