#include <iostream>
#include <assert.h>
#include <stdint.h>
#include <zmq.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main(int argc, char** argv)
{
  void *context = zmq_ctx_new ();
  void *subscriber = zmq_socket (context, ZMQ_SUB);
  int  rhwm = 0;
  
  if (argc < 2)
    {
      cout << "-USAGE: " << argv[0] << " <uint log print modulo>" << endl;
      abort();
    }

  uint64_t mod = atoi(argv[1]);

  int rcso = zmq_setsockopt (subscriber, ZMQ_RCVHWM, &rhwm, sizeof(rhwm));
  cout << "RCSO = " << rcso << endl;
  //  assert(rcso == 0);

  if (!rcso)
    {
      cout << "Error: " << zmq_strerror (rcso) << endl;
    }

  //int rcc = zmq_connect(subscriber, "tcp://127.0.0.1:5563");
  int rcc = zmq_connect(subscriber, "ipc:///var/tmp/pub");

  assert(rcc == 0);

  int rc = zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "", strlen(""));
  assert (rc == 0);
  uint64_t c = 0;

  while (true)
    {
      zmq_msg_t msg;
      int rc = zmq_msg_init(&msg);
      assert(rc == 0);

      int rcr  = zmq_msg_recv (&msg, subscriber, 0);
      assert (rcr != -1);
      if (++c % mod == 0)
        {
          string rpl = std::string(static_cast<char*>(zmq_msg_data(&msg)),
                                       zmq_msg_size(&msg));


          cout << "RECV:" << c << "|" << rpl << endl;
        }
      zmq_msg_close(&msg);
    }
}
