#include <iostream>
#include <sstream>
#include <assert.h>
#include <stdint.h>
#include <zmq.h>
#include <time.h>
#include <ostream>
#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main(int argc, char** argv)
{
  void *context = zmq_ctx_new ();
  void *publisher = zmq_socket (context, ZMQ_PUSH);
  int rhwm = 0;
  uint64_t c = 1;

  if (argc < 3)
    {
      cout << "-USAGE: " << argv[0] << " <uint number of iterations> <uint log print modulo>" << endl;
      cout << "  -e.g. to send 100,000 messages and log every 100: $" << argv[0] << " 100000 100" << endl;
      abort();
    }

  uint64_t loop = atoi(argv[1]);

  int rcso = zmq_setsockopt (publisher, ZMQ_SNDHWM, &rhwm, sizeof(rhwm));
  if (!rcso)
    {
      cout << "Error: " << zmq_strerror (rcso) << endl;
    }

  zmq_bind (publisher, "tcp://*:5563");

  sleep (1); // idle to ensure connection made to subscriber

  while (c <= loop)
    {
      std::stringstream ss;
      ss << "MESSAGE PAYLOAD OF A NONTRIVAL SIZE KIND OF AND SUCH #" << c;
      zmq_msg_t msg;
      int rc1 = zmq_msg_init_size(&msg, ss.str().length());
      assert(rc1 == 0);
      memcpy (zmq_msg_data (&msg), ss.str().c_str(), ss.str().length());

      int rc = zmq_msg_send(&msg, publisher, 0);

      if (c % atoi(argv[2]) == 0)
         cout << "sent " << ss.str() << " with rc="<< rc << endl;

      if (!rc)
        {
          cout << "Error: " << zmq_strerror (rc) << endl;
          abort();
        }

      ++c;
    }
  zmq_close(publisher);
  zmq_ctx_destroy(context);  
}
